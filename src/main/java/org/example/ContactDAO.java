package org.example;

import java.util.ArrayList;
import java.util.List;

public class ContactDAO {

    private List<String> emails = new ArrayList<>();

    public void add(String email) throws DataDuplicationException {
        if (!this.emails.contains(email)) {
            this.emails.add(email);
        } else {
            throw new DataDuplicationException("Le mail " + email + " est déjà présent dans la liste");
        }
    }

    public List<String> getEmails() {
        return emails;
    }

    public boolean deleteEmail(String email) {
        return emails.remove(email);
    }

}
