package org.example;

import java.util.List;

public class ContactService {

    private ContactDAO contactDAO = new ContactDAO();

    public void addContact(String email) {
        if (EmailValidator.isValid(email)) {
            try {
                contactDAO.add(email);
            } catch (DataDuplicationException e) {
                throw new RuntimeException("email " + email + " is already in the list");
            }
        } else {
            throw new RuntimeException("L'email est invalide");
        }
    }

    public void deleteContact(String email) {
        if (EmailValidator.isValid(email)) {
            if (!contactDAO.deleteEmail(email)) {
                throw new RuntimeException("L'email n'est pas dans la liste");
            }
        } else {
            throw new RuntimeException("L'email n'est pas valide");
        }
    }

    public List<String> getContacts() {
        return contactDAO.getEmails();
    }

}
