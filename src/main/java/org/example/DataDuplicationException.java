package org.example;

public class DataDuplicationException extends Exception {
    public DataDuplicationException(String message) {
        super(message);
    }
}
