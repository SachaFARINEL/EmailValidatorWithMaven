import org.example.ContactService;
import org.example.DataDuplicationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ContactServiceTest {

    private final ContactService contactService = new ContactService();

    @Test
    void shouldInsertValidEmail() {
        Assertions.assertDoesNotThrow(() -> contactService.addContact("farinel.sacha@gmail.com"));
    }

    @Test
    void shouldRefuseInvalidEmail() {
        Assertions.assertThrows(RuntimeException.class, () -> contactService.addContact("sacha"));
    }

    @Test
    void shouldRaiseExceptionOnDuplicatedEmail() throws DataDuplicationException {
        contactService.addContact("farinel.sacha@gmail.com");
        Assertions.assertThrows(RuntimeException.class, () -> contactService.addContact("farinel.sacha@gmail.com"));
    }

}