import org.example.EmailValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EmailValidatorTest {

    @Test
    void testIsValidTrue() {
        Assertions.assertTrue(EmailValidator.isValid("farinel.sacha@gmail.com"));
    }

    @Test
    void testIsValidFalse() {
        Assertions.assertFalse(EmailValidator.isValid("farinel.sachagmail.com"));
    }

    @Test
    void testIsValidFalseMissingPoint() {
        Assertions.assertFalse(EmailValidator.isValid("farinel.sacha@gmailcom"));
    }

}