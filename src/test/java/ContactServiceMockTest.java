import org.example.ContactDAO;
import org.example.ContactService;
import org.example.DataDuplicationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoSettings;

@MockitoSettings
class ContactServiceMockTest {

    @InjectMocks
    private ContactService service = new ContactService();

    @Mock
    private ContactDAO contactDAO;

    @Test
    void shouldTest() throws DataDuplicationException {
        Mockito.doThrow(new DataDuplicationException("error"))
                .when(contactDAO)
                .add("test@gmail.com");

        Assertions.assertThrows(RuntimeException.class,
                () -> service.addContact("test@gmail.com"));
    }

    @Test
    void testDeleteContact() {
        Mockito.doReturn(false)
                .when(contactDAO)
                .deleteEmail(Mockito.anyString());

        Assertions.assertThrows(RuntimeException.class,
                () -> service.deleteContact("test@gmail.com"));
    }

}
